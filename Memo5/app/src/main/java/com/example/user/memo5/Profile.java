package com.example.user.memo5;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class Profile extends Fragment {
    TextView UserName;
    ImageView UserViewUst,ImageButtonOrta;
    ImageButton ımageButtonAdresler;

    public Profile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view= inflater.inflate(R.layout.fragment_profile, container, false);
        SharedPreferences sharedPref = getContext().getSharedPreferences("gizli", 0);
        String savedAdSoyad = sharedPref.getString("adSoyad","Kayıt Yok");
        String savedResim = sharedPref.getString("resim","Kayıt Yok");
        String savedSifre= sharedPref.getString("sifreTutulan","Kayıt Yok");
        String savedKadi= sharedPref.getString("kadiNav","Kayıt Yok");
        int savedid = sharedPref.getInt("id",0);
       // ımageButtonAdresler = (ImageButton) view.findViewById(R.id.bilgileriDuzenle);


      /*  UserName = (TextView) view.findViewById(R.id.user_profile_name);
        UserName.setText(savedAdSoyad);*/
        UserName= (TextView) view.findViewById(R.id.user_profile_name);
        UserName.setText(savedAdSoyad);

        UserViewUst =(ImageView) view.findViewById(R.id.imageViewUst);
        ImageButtonOrta=(ImageView) view.findViewById(R.id.imageButtonOrta);
        Picasso.with(ImageButtonOrta.getContext())
                .load("https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAjBAAAAJDBhYjkzNDdhLTJiNzAtNDk0OC1iOThkLTc3NjA3NzA0NGM5OA.jpg")
                .fit()
                .centerCrop()
                .into(ImageButtonOrta);
        UserViewUst.setImageResource(R.mipmap.userbaslik);

       /* Picasso.with(UserViewUst.getContext())
                .load("https://3.bp.blogspot.com/-xIushOCFDWM/VuZxKaEX_rI/AAAAAAAACv8/OAdrzwF03aAAYUR2uLXLu7buujfZleR5g/w1200-h630-p-nu/material-design-android-profile-screen-xml-ui-design.png")
                .fit()
                .centerCrop()
                .into(UserViewUst);*/
        return view;

    }

}
