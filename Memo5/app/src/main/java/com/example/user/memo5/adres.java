package com.example.user.memo5;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class adres extends Fragment {


    Button adresKayıt, adresSil;
    EditText adresBaslik,adresAdres;
    String alinanBaslik,alinanAdres,yazilanAdres,yazilanBaslik;
    String url="http://eticaret.merkezyazilim.com/service/adresler/ekle";
    String url2="http://eticaret.merkezyazilim.com/service/adresler/sil";
    public adres() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view=  inflater.inflate(R.layout.fragment_adres, container, false);

        SharedPreferences sp = getActivity().getSharedPreferences("gizli", 0);
        String adSoyad = sp.getString("adSoyad", "YourName");
        int id = sp.getInt("id", 1);
        String resim = sp.getString("resim", "resim");
        final String sifre = sp.getString("sifreTutulan", "sifreTutulan");
        final String kadi = sp.getString("kadiNav", "kadiNav");
       // Toast.makeText(getActivity(), kadi+" "+sifre+" "+id+" "+adSoyad, Toast.LENGTH_LONG).show();




        adresAdres = (EditText) view.findViewById(R.id.adres);
        adresBaslik= (EditText) view.findViewById(R.id.baslik);
        adresKayıt= (Button) view.findViewById(R.id.dialogkaydet);
        adresSil= (Button) view.findViewById(R.id.dialogsil);
        alinanBaslik=adresBaslik.getText().toString().trim();
        alinanAdres=adresAdres.getText().toString().toString();
        adresKayıt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(),"selamlar", Toast.LENGTH_LONG).show();
                RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
                StringRequest stringRequest = new StringRequest( url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();

                        try {
                            JSONObject obj= new JSONObject(response);
                            yazilanAdres=obj.getString("adres");
                            yazilanBaslik=obj.getString("baslik");
                            Toast.makeText(getActivity(), yazilanBaslik+" "+yazilanAdres, Toast.LENGTH_LONG).show();

                        }catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                            // alertMesaj("Bir problemle karşılaştık, Hatayı bize bildirin");
                        }


                        // Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
                        //Log.d("TAG", "hello");
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();

                        params.put("kadi",kadi);
                        params.put("sifre",sifre);
                        params.put("adres",alinanAdres);
                        params.put("baslik",alinanBaslik);
                        //Toast.makeText(getActivity(), yazilanBaslik+" "+yazilanAdres, Toast.LENGTH_LONG).show();


                        return params;
                    } @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");
                        return params;
                    }
                };
                queue.add(stringRequest);
            }
        });
        return view;
    }

}
