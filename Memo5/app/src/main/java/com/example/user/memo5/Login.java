package com.example.user.memo5;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class Login extends Activity {

    EditText kadi,sifre;
    Button giris,kayit;
    String loginKadi,loginSifre,soyadiNav,adiNav,adSoyad,kadiNav;
    String ErrorString="",AlertString,mesaj,resim,sifreTutulan;
    int durum ,id ;
    int tip ;
    static  Login activityA;
    AlertDialog.Builder builder;
    Bundle bundle;
    ImageView loginView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        kadi=(EditText)findViewById(R.id.kadi);
        sifre=(EditText)findViewById(R.id.sifre);
        giris=(Button)findViewById(R.id.giris);
        kayit=(Button)findViewById(R.id.kayit);

        final SharedPreferences sharedPref = getSharedPreferences("gizli", Context.MODE_PRIVATE);

        builder = new AlertDialog.Builder(Login.this);
        builder.setTitle("Kel Amca");
        builder.setMessage(AlertString);

        builder.setPositiveButton("TAMAM", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Tamam butonuna basılınca yapılacaklar

            }
        });

        loginView = (ImageView) findViewById(R.id.ResimLogin);
        loginView.setImageResource(R.mipmap.kapak);

       /* Picasso.with(loginView.getContext())
                .load("https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSI7nLbmZ2zCCQITT5MSZpEKhaqLPY5Snnh0FXRWN70xbQK60AZ")
                .fit()
                .centerCrop()
                .into(loginView);*/



        kayit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kayitaGit = new Intent(getApplicationContext(),Register.class);
                startActivity(kayitaGit);
            }
        });

        giris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loginKadi = kadi.getText().toString().trim();
                loginSifre = sifre.getText().toString().trim();


                if (loginKadi.matches("") || loginSifre.matches("")) {
                    ErrorString = "";
                    if (loginKadi.matches("")) {

                        ErrorString += "* Kullanıcı Adı Boş Lütfen Doldorunuz\n";

                    }
                    if (loginSifre.matches("")) {

                        ErrorString += "* Şifre Boş Lütfen Doldorunuz\n";

                    }



                    alertMesaj(ErrorString);

                }else{
                    //  Toast.makeText(Register.this, "Girdi", Toast.LENGTH_SHORT).show();
                    RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                    StringRequest sr = new StringRequest(Request.Method.POST, "http://eticaret.merkezyazilim.com/service/giris", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {


                            try {

                                //verileri sametten alıyorum

                                JSONObject obj = new JSONObject(response);
                                durum = obj.getInt("durum");
                                tip = obj.getInt("type");
                                mesaj = obj.getString("mesaj");

                                //verileri aktiviteler arası yollamak için ekleme yaptım
                                kadiNav = obj.getString("kadi");
                                adiNav = obj.getString("adi");
                                soyadiNav = obj.getString("soyadi");
                                adSoyad=adiNav +" "+ soyadiNav;
                                resim = obj.getString("resim");
                                id=obj.getInt("id");
                                sifreTutulan=obj.getString("sifre");

                                //alertMesaj(resim);




                                if (durum == 0) {

                                    if (tip == 1) {
                                        alertMesaj("Veriler Gönderilemedi Lütfen Daha Sonra Tekrar Deneyin");

                                    } else if (tip == 2) {

                                        alertMesaj(mesaj);

                                    } else if (tip == 3) {

                                        alertMesaj("Sunucuya Erişilemedi");
                                    } else if (tip == 4) {
                                        alertMesaj("Kullanıcı Adı ve Şifre Hatalı");

                                    } else if (tip == 5) {

                                        alertMesaj(mesaj);
                                    }

                                } else if (durum == 1) {

                                    if (tip == 0) {

                                        //alertMesaj("Giriş Başarılı Bro");


                                        SharedPreferences.Editor editor = sharedPref.edit();

                                        editor.putString("adSoyad",adSoyad); //string değer ekleniyor
                                        editor.putInt("id",id); //int değer ekleniyor
                                        editor.putString("resim",resim); //string değer ekleniyor
                                        editor.putString("sifreTutulan",sifreTutulan); //string değer ekleniyor
                                        editor.putString("kadiNav",kadiNav); //string değer ekleniyor
                                        editor.commit();
                                        Intent LogindenAnasayfaya = new Intent(getApplicationContext(), Anasayfa.class);
                                        //verileri aktiviteler arası yollama
                                        bundle =new Bundle();
                                        LogindenAnasayfaya.putExtra("gonderlinKadi",kadiNav);
                                        LogindenAnasayfaya.putExtra("gonderilenadSoyad",adSoyad);
                                        LogindenAnasayfaya.putExtra("gonderilenResim",resim);

                                        startActivity(LogindenAnasayfaya);

                                    }

                                }


                                Log.d("My App", obj.toString());

                            } catch (Throwable t) {
                                Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                                alertMesaj("Bir problemle karşılaştık, Hatayı bize bildirin");
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            alertMesaj("SQLite ile hata durumları eklenecek");
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();

                            //şifre ve kadi yi webservice ye yolluyorum
                            params.put("kadi", loginKadi);
                            params.put("sifre", loginSifre);

                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Content-Type", "application/x-www-form-urlencoded");
                            return params;
                        }
                    };
                    queue.add(sr);

                }}


        }); }


    public static Login getInstanceRegister(){
        return   activityA;
    }

    public void alertMesaj(String mesaj){

        builder.setMessage(mesaj);
        builder.show();

    }

    @Override
    protected void onPause() {
        super.onPause();
        this.finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.finish();
    }


}

