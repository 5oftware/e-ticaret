package com.example.user.memo5;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.android.volley.VolleyLog.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class UrunAra extends Fragment {

    private ProgressDialog pDialog;
    private ListView lv;

    private static String url = "http://eticaret.merkezyazilim.com/service/urunler/index/0";

    ArrayList<HashMap<String, String>> contactList;

    public UrunAra() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_urun_ara, container, false);
        contactList = new ArrayList<>();

        lv = (ListView)view.findViewById(R.id.listurun);
        new GetContacts().execute();
        return view;
    }


    private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Lütfen Bekleyin...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    JSONArray urunler = jsonObj.getJSONArray("urunler");


                    for (int i = 0; i < urunler.length(); i++) {
                        JSONObject c = urunler.getJSONObject(i);
                        String id = c.getString("id");
                        String baslik = c.getString("baslik");
                        String resmi=c.getString("resmi");
                        String kisaaciklama=c.getString("kisaaciklama");


                        HashMap<String, String> contact = new HashMap<>();


                        contact.put("id", id);
                        contact.put("baslik", baslik);
                        contact.put("resmi", resmi);
                        contact.put("kisaaciklama", kisaaciklama);


                        contactList.add(contact);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());


                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");


            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();

            ListAdapter adapter = new SimpleAdapter(
                    getContext(), contactList,
                    R.layout.list_item_urun, new String[]{"baslik","kisaaciklama" }, new int[]{R.id.baslik, R.id.kisaaciklama});

            lv.setAdapter(adapter);
        }

    }
}
