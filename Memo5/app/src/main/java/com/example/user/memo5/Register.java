package com.example.user.memo5;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Register extends Activity {

    EditText isim, soyisim,kullaniciAdi,sifre,eMail,telefon;
    Button kayit;
    String soyadiNav,kadiNav,adiNav,adSoyad,resim,sifreTutulan;
    String ErrorString="",RegisterAdi,RegisterSoyadi,RegisterKadi,RegisterSifre,RegisterEmail,RegisterTelno,AlertString,mesaj;
    int durum ,id ;
    int tip ;
    AlertDialog.Builder builder;
    static Register activityA;
    Bundle bundle;
    ImageView imageRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final SharedPreferences sharedPref = getSharedPreferences("gizli", Context.MODE_PRIVATE);

        activityA = this;

        Object fa = this;
        isim=(EditText)findViewById(R.id.isim);
        soyisim=(EditText)findViewById(R.id.soyisim);
        kullaniciAdi=(EditText)findViewById(R.id.kullaniciAdi);
        sifre=(EditText)findViewById(R.id.sifre);
        eMail=(EditText)findViewById(R.id.eMail);
        telefon=(EditText)findViewById(R.id.telefon);
        kayit=(Button)findViewById(R.id.kayit);


        imageRegister = (ImageView) findViewById(R.id.ResimRegister);

       /* Picasso.with(imageRegister.getContext())
                .load("https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSI7nLbmZ2zCCQITT5MSZpEKhaqLPY5Snnh0FXRWN70xbQK60AZ")
                .fit()
                .centerCrop()
                .into(imageRegister);*/

        imageRegister.setImageResource(R.mipmap.kapak);

        builder = new AlertDialog.Builder(Register.this);
        builder.setTitle("Kel Amca");
        builder.setMessage(AlertString);


        builder.setPositiveButton("TAMAM", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Tamam butonuna basılınca yapılacaklar

            }
        });


        kayit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterAdi=isim.getText().toString().trim();
                RegisterSoyadi=soyisim.getText().toString().trim();
                RegisterKadi=kullaniciAdi.getText().toString().trim();
                RegisterSifre=sifre.getText().toString().trim();
                RegisterEmail=eMail.getText().toString().trim();
                RegisterTelno=telefon.getText().toString().trim();

                if(RegisterEmail.matches("") || RegisterAdi.matches("") ||RegisterSoyadi.matches("") || RegisterSifre.matches("") || RegisterTelno.matches("")   ){
                    ErrorString="";
                    if(RegisterEmail.matches("")){

                        ErrorString += "* Email Boş Lütfen Doldorunuz\n";

                    }
                    if(RegisterAdi.matches("")){

                        ErrorString += "* Adiniz Boş Lütfen Doldorunuz\n";

                    }
                    if(RegisterSoyadi.matches("")){

                        ErrorString += "* Soyadiniz Boş Lütfen Doldorunuz\n";

                    }
                    if(RegisterSifre.matches("")){

                        ErrorString += "* Şifreniz Boş Lütfen Doldorunuz\n";

                    }
                    if(RegisterTelno.matches("")){

                        ErrorString += "* Telefon Numarnaız Boş Lütfen Doldorunuz\n";

                    }

                    alertMesaj(ErrorString);

                }
                else{
                    //  Toast.makeText(Register.this, "Girdi", Toast.LENGTH_SHORT).show();
                    RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                    StringRequest sr = new StringRequest(Request.Method.POST,"http://eticaret.merkezyazilim.com/service/kayit", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {


                            try {
                                //verileri sametten alıyorum

                                JSONObject obj = new JSONObject(response);
                                durum = obj.getInt("durum");
                                tip=obj.getInt("type");
                                mesaj=obj.getString("mesaj");

                                //verileri aktiviteler arası yollamak için ekleme yaptım
                                kadiNav = obj.getString("kadi");
                                adiNav = obj.getString("adi");
                                soyadiNav = obj.getString("soyadi");
                                adSoyad=adiNav +" "+ soyadiNav;
                                id=obj.getInt("id");
                                resim=obj.getString("resim");
                                sifreTutulan=obj.getString("sifre");




                                SharedPreferences.Editor editor = sharedPref.edit(); //SharedPreferences'a kayıt eklemek için editor oluşturuyoruz
                                editor.putString("adSoyad",adSoyad); //string değer ekleniyor
                                editor.putInt("id",id); //string değer ekleniyor
                                editor.putString("resim",resim); //string değer ekleniyor
                                editor.putString("sifreTutulan",sifreTutulan); //string değer ekleniyor
                                editor.putString("kadiNav",kadiNav); //string değer ekleniyor
                                editor.commit();

                                if(durum==0){

                                    if(tip==1){
                                        alertMesaj("Veriler Gönderilemedi Lütfen Daha Sonra Tekrar Deneyin");

                                    }else if(tip==2){

                                        alertMesaj(mesaj);

                                    }else if(tip==3){

                                        alertMesaj("Sunucuya Erişilemedi");
                                    }
                                    else if(tip==4){
                                        alertMesaj("Kullanıcı Adı ve Şifre Hatalı");

                                    }else if(tip==5){

                                        alertMesaj(mesaj);
                                    }

                                }else if(durum==1){

                                    if(tip==0){

                                        // alertMesaj("Giriş Başarılı Bro");



                                        Intent RegisterdenAnasayfaya = new Intent(getApplicationContext(), Anasayfa.class);



                                        //verileri aktiviteler arası yollama
                                       bundle =new Bundle();
                                      RegisterdenAnasayfaya.putExtra("gonderlinKadi",kadiNav);
                                       RegisterdenAnasayfaya.putExtra("gonderilenadSoyad",adSoyad);
                                        startActivity(RegisterdenAnasayfaya);
                                    }

                                }


                                Log.d("My App", obj.toString());

                            } catch (Throwable t) {
                                Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                                alertMesaj("Bir problemle karşılaştık, Hatayı bize bildirin");
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            alertMesaj("SQLite ile hata durumları eklenecek");
                        }
                    }){
                        @Override
                        protected Map<String,String> getParams(){
                            //verileri samete yolluyorum
                            Map<String,String> params = new HashMap<String, String>();
                            params.put("adi",RegisterAdi);
                            params.put("soyadi",RegisterSoyadi);
                            params.put("kadi",RegisterKadi);
                            params.put("sifre",RegisterSifre);
                            params.put("email",RegisterEmail);
                            params.put("tel",RegisterTelno);

                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String,String> params = new HashMap<String, String>();
                            params.put("Content-Type","application/x-www-form-urlencoded");
                            return params;
                        }
                    };
                    queue.add(sr);

                }

            }
        });

    }

    public static Register getInstanceRegister(){
        return   activityA;
    }

    public void alertMesaj(String mesaj){

        builder.setMessage(mesaj);

        builder.show();

    }

    @Override
    protected void onPause() {
        super.onPause();
        this.finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.finish();
    }
}
