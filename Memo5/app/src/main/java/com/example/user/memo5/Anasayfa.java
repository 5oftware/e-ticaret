package com.example.user.memo5;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class Anasayfa extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ImageView imageView;
    TextView kadiNav,emailNav;
    Bundle veri;
    String gelenKadi,gelenAdsoyad,gelenResim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences sharedPref = getSharedPreferences("gizli",0);
        String savedAdSoyad = sharedPref.getString("adSoyad","Kayıt Yok");
        String savedResim = sharedPref.getString("resim","Kayıt Yok");
        String savedSifre= sharedPref.getString("sifreTutulan","Kayıt Yok");
        String savedKadi= sharedPref.getString("kadiNav","Kayıt Yok");
        int savedid = sharedPref.getInt("id",0);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anasayfa);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        //degerleri nav da ki isimsoyisim ve kullanıcı adı olarak yazdım
        kadiNav= (TextView) findViewById(R.id.kadiNav);
        emailNav= (TextView) findViewById(R.id.emailNav);
        veri =getIntent().getExtras();
        gelenKadi =veri.getString("gonderlinKadi");
        gelenAdsoyad =veri.getString("gonderilenadSoyad");
        kadiNav.setText(gelenAdsoyad);
        emailNav.setText(gelenKadi);
        gelenResim=veri.getString("gonderilenResim");


        //resimleri alıyorum
        imageView = (ImageView) findViewById(R.id.imageViewNav);
        Picasso.with(imageView.getContext())
                .load("https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSMDlI3z3KYSdHApFHeyi1tXtQfLz40X8aNOv2It2dUTFhSvpZ_f07C4Ck")
                .resize(70,70)
                .into(imageView);

        getMenuInflater().inflate(R.menu.anasayfa, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        FragmentManager manager = getSupportFragmentManager();
        //noinspection SimplifiableIfStatement
        if  (id == R.id.action_settings) {
            Intent kayitaGit = new Intent(getApplicationContext(),Ayarlar.class);
            startActivity(kayitaGit);
            // Handle the camera action
        }else  if (id == R.id.action_exit) {
            this.finish();

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        FragmentManager manager = getSupportFragmentManager();

        if (id == R.id.anasayfa) {

        } else if (id == R.id.nav_profile) {
            manager.beginTransaction()
                    .replace(R.id.content_anasayfa
                            , new Profile())
                    . commit();

        } else if (id == R.id.urunAra) {
            manager.beginTransaction()
                    .replace(R.id.content_anasayfa
                            , new UrunAra())
                    . commit();

        } else if (id == R.id.banaOzel) {
            manager.beginTransaction()
                    .replace(R.id.content_anasayfa
                            , new BanaOzel())
                    .commit();

        } else if (id == R.id.nav_favori) {
            manager.beginTransaction()
                    .replace(R.id.content_anasayfa
                            , new Favoriler())
                    .commit();

        }
        else if (id == R.id.nav_siparisler) {
            manager.beginTransaction()
                    .replace(R.id.content_anasayfa
                            , new Siparisler())
                    . commit();


        } else if (id == R.id.kategoriler) {
            manager.beginTransaction()
                    .replace(R.id.content_anasayfa
                            , new Kategoriler())
                    . commit();

        }
        else if (id == R.id.nav_adres) {
            manager.beginTransaction()
                    .replace(R.id.content_anasayfa
                            , new adres())
                    . commit();

        }
        else if (id == R.id.nav_ayarlar ) {
            Intent kayitaGit = new Intent(getApplicationContext(),Ayarlar.class);
            startActivity(kayitaGit);


        }
        else if (id == R.id.nav_cıkıs) {

            this.finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}